#include "MotorVehicle.h"
using namespace std;
#include <cmath>
#include <iostream>

MotorVehicle::MotorVehicle(double capacity, double consumption, MotorType kindOfEnergy)
	: traction(kindOfEnergy)
	, consumption(consumption)
	, capacity(capacity)
	, startReserves(0.0)
{
}


MotorVehicle::~MotorVehicle()
{
}

double MotorVehicle::getPosition(Length::Unit unit) const
{
	double pos = this->Vehicle::getPosition();
	double dist = pos - this->startPosition;	// signed!
	double reach = this->getReach();
	if (abs(dist) > reach) {
		pos = this->startPosition + (dist < 0 ? -1 : 1) * reach;
	}
	return Length(pos).getValue(unit);
}

double MotorVehicle::getSpeed() const
{
	double velocity = this->Vehicle::getSpeed();
	double dist = this->Vehicle::getPosition() - this->startPosition;	// signed!
	double reach = this->getReach();
	if (abs(dist) > reach) {
		velocity = 0.0;
	}
	return velocity;
}

void MotorVehicle::setSpeed(double newSpeed)
{
	double remaining = getReserves();
	this->Vehicle::setSpeed(newSpeed);
	this->startReserves = remaining;
}

double MotorVehicle::getReserves() const
{
	double dist = abs(this->getPosition() - this->startPosition);
	double consumed = dist * this->consumption;
	return this->startReserves - consumed;
}

void MotorVehicle::save(std::ofstream & _fout) const
{
	_fout << "m" << endl;
	_fout << this->traction << endl;
	_fout << this->capacity << endl;
	_fout << this->startReserves << endl;
	_fout << this->consumption << endl;
	_fout << this->startPosition << endl;
	_fout << this->getSpeed() << endl;
	
}

void MotorVehicle::load(std::ifstream & _fin)
{
	string linePart;
	double number = 0.0;
	getline(_fin, linePart);
	istringstream iss(linePart);
	iss >> this->startPosition;
	getline(_fin, linePart);
	istringstream iss1(linePart);
	iss1 >> number;
	this->setSpeed(number);

}

double MotorVehicle::getReach() const
{
	return this->startReserves / this->consumption;
}
