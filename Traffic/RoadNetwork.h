#pragma once
#ifndef ROADNETWORK_H
#define ROADNETWORK_H

#include <map>
#include <string>
#include "Drawer2D.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include "MotorVehicle.h"
class Junction;
class Road;

using std::map;
using std::multimap;
using std::string;

class RoadNetwork
{
public:
	RoadNetwork();
	~RoadNetwork();
	// Adds the given junction to the road network if its name hadn't
	// been registered with another junction before.
	// Returns: true if the junction was accepted, false otherwise
	bool addJunction(Junction& junction);
	// Adds the given road to the road network
	// Returns: true if the road was accepted, false otherwise
	bool addRoad(Road& road);
	// Retrieves a junction with given name. If such a junction is
	// registered then its address wil be returned, otherwise nullptr
	Junction* getJunction(string name) const;

	// Draws the entire network (all roads and junctions [and vehicles])
	void draw(Drawer2D& drawer) const;
	void save(const char* path)const;
	void load(const char* path);

private:
	void saveVehicles(std::ifstream& _fin, bool toFro, int number, Road& road);
	MotorVehicle::MotorType typ(int _nump);

	map<string, Junction*> junctions;//vse, kotorie v roads i + paar
	multimap<string, Road*> roads;
	//string name?


	//map < string, shared_ptr<Junction>> hz;
	//shared_ptr<Junction> jptr(new Junction);
	
};

#endif /*ROADNETWORK_H*/