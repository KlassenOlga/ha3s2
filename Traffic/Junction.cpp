#include "Junction.h"
#include "Road.h"
#include "RoadNetwork.h"

#include <fstream>
#include <iostream>
#include <string>


#include <cstdlib>
#include <ios>
using namespace std;
Junction::Junction(const Point2D& location, string name, RoadNetwork& network)
	: m_name(name)
	, location(location)
	, pNetwork(&network)
{
	// TODO Reaktion auf false?
	network.addJunction(*this);
}



Junction::~Junction()
{
	// Abmelden von Netzwerk

}



void Junction::connectTo(Road & road, bool incoming)
{
	if (incoming) {
		this->incomingRoads.push_back(&road);
	}
	else {
		this->outgoingRoads.push_back(&road);
	}
}

string Junction::getName() const
{
	return this->m_name;
}

Point2D Junction::getLocation() const
{
	return this->location;
}

RoadNetwork * Junction::getNetwork() const
{
	return this->pNetwork;
}

void Junction::draw(Drawer2D & drawer) const
{
	this->location.draw(drawer);
}

void Junction::save(std::ofstream& fout) const
{
	

	fout << this->m_name << endl;
	this->location.save(fout);
	 
}


