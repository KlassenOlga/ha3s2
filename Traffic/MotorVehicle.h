#pragma once
#include "Vehicle.h"
#include <sstream>
class MotorVehicle :
	public Vehicle
{
public:
	enum MotorType {PETROL, DIESEL, LPG, HYDROGEN, ELECTRO};
	MotorVehicle(double capacity, double consumption, MotorType kindOfEnergy = ELECTRO);
	~MotorVehicle();
	// Retrieves the current position (distance from 0) in the given length unit
	virtual double getPosition(Length::Unit unit = Length::METRE) const;
	// Returns the current velocity in metres per second
	virtual double getSpeed() const;
	// Changes the velocity to the given speed in metres per second
	void setSpeed(double newSpeed);
	// Returns the current energy reserves (in appropriate units)
	double getReserves() const;
	
	virtual void save(std::ofstream& _fout)const override;
	virtual void load(std::ifstream& _fin)override;
private:
	const MotorType traction;
	// Maximum amount of carriable energy reserves
	const double capacity;
	// Energy reserves at start of the current movement phase
	double startReserves;
	// Average energy consumption per metre
	const double consumption;
	// Returns the action radius with given energy reserves in metres
	double getReach() const;
};

