#include "Length.h"

const double Length::factors[] = { 0.001, 0.01, 0.3048, 0.9144, 1.0, 1000.0, 1609.3 };

Length::Length(double val, Unit unit)
	: value(val * factors[unit])
{
}

Length::~Length()
{
}

double Length::getValue(Unit unit)
{
	return value / factors[unit];
}
