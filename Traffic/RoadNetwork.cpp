#include "RoadNetwork.h"
#include "Junction.h"
#include "Road.h"



using namespace std;
RoadNetwork::RoadNetwork()
{
}


RoadNetwork::~RoadNetwork()
{
	//Network ist selbst f�r seine Strassen und Knoten zust�ndig
	for (auto it=junctions.begin(); it!=junctions.end();++it )
	{
		Junction* j = it->second;

		delete j;

	}
	junctions.clear();

	for (auto it=roads.begin(); it!=roads.end(); ++it)
	{
		if (it->second!=nullptr)
		{
			Road* r = it->second;
			delete r;
			r = nullptr;
		}
	}
	roads.clear();
}

bool RoadNetwork::addJunction(Junction & junction)
{
	bool done = false;
	if (this->junctions.find(junction.getName()) == this->junctions.end()) {
		this->junctions.insert(map<string,Junction*>::value_type(junction.getName(), &junction));
		//this->junctions[junction.getName()] = &junction;	// So w�re es auch gegangen
		done = true;
	}
	return done;
}

bool RoadNetwork::addRoad(Road & road)
{
	auto iterRange = this->roads.equal_range(road.getName());
	for (multimap<string, Road*>::iterator iter = iterRange.first;
		iter != iterRange.second;
		++iter) {
		if (iter->second == &road) {
			return false;
		}
	}
	this->roads.insert(multimap<string, Road*>::value_type(road.getName(), &road));
	//this->roads.insert({ road.getName(), &road });
	return true;
}

Junction * RoadNetwork::getJunction(string name) const
{
	auto iter = this->junctions.find(name);
	if (iter==this->junctions.end())
	{
		return nullptr;
	}
	return iter->second;
	
}

void RoadNetwork::draw(Drawer2D & drawer) const
{
	for (auto iter = this->roads.begin(); iter != this->roads.end(); ++iter) {
		iter->second->draw(drawer);
	}
	for (auto iter = this->junctions.begin(); iter != this->junctions.end(); ++iter) {
		iter->second->draw(drawer);
	}
}

void RoadNetwork::save(const char * _path)const
{

	ofstream fout(_path);
	
	
	if (!fout.is_open())
	{
		throw "File can't be opened";
	}

	fout << junctions.size()<<endl;
	
	for (auto i = junctions.begin(); i != junctions.end(); ++i)
	{
		i->second->save(fout);
	}

	fout << roads.size()<<endl;
	for (auto i=roads.begin(); i!=roads.end(); ++i)
	{
		i->second->save(fout);
	}

}

void RoadNetwork::load(const char* path)
{

	ifstream _fin(path);


	if (!_fin.is_open())
	{
		throw "File can't be opened";
	}


	int numberOjJunctions = 0;
	int numberOfRoads = 0;
	int number = 0;
	string linePart;
	string linePart1;
	string nameJunction;
	string nameRoad;
	bool forward;
	bool backward;

	getline(_fin, linePart);//entnehme die Anzahl von Knoten
	istringstream iss(linePart);
	iss >> numberOjJunctions;


	for (int i = 0; i < numberOjJunctions; i++) {
		getline(_fin, nameJunction);//entnehme den Name der Knote
		Point2D pointJunction = Point2D::load(_fin);//kriege den Punkt der Knote

		//Es wird ein Junction erzogen und gleichzeitig wegen der Verbindung an Network in map hingef�gt 
		Junction* newJuntion = new Junction(pointJunction, nameJunction, *this);
																				

	}

	getline(_fin, linePart);//entnehme die ANzahl von Roads
	stringstream iss1(linePart);
	iss1 >> numberOfRoads;

	string nameStartKnote;
	string nameEndKnote;
	for (int i = 0; i < numberOfRoads; i++)
	{
			
		Polyline2D py = Polyline2D::load(_fin);//kriege Polylinie der Strasse
		getline(_fin, nameRoad);//kriege den Name der Strasse 

		getline(_fin, linePart);
		istringstream iss(linePart);//erfahre, ob die Strasse backward ist
		iss >> backward;

		getline(_fin, linePart);
		istringstream iss1(linePart);//erfahre, ob die Strasse forward ist
		iss1 >>forward;

		
		getline(_fin, nameStartKnote);//entnehme den Name der StartKnote
		getline(_fin, nameEndKnote);//entnehme den Name der EndKnote


		Road* road = new Road(*getJunction(nameStartKnote), *getJunction(nameEndKnote),py, forward, backward,nameRoad);
		//Liste von Roads in Junctions werden automatisch im Roadkonstruktor gemacht!



	///////////////////////////////////////////////////////////////////////////////////////////////////////
											//VehiclesZeug//						
// 
		getline(_fin, linePart);//kriege die Anzahl von Fahrzeuge f�r die Liste VehiclesTo 
		istringstream iss2(linePart);
		iss2 >> number;
		saveVehicles(_fin, 1, number, *road);

		getline(_fin, linePart);//kriege die Anzahl von Fahrzeuge f�r die Liste VehiclesTo 
		istringstream iss3(linePart);
		iss2 >> number;
		saveVehicles(_fin, 0, number, *road);

	}
	

}

void RoadNetwork::saveVehicles(std::ifstream & _fin, bool toFro, int number, Road & road)
{

	for (number; number > 0; number--)
	{
		string linePart;
		getline(_fin, linePart);//entnehme flag: m oder v
		if (linePart == "v")
		{
			Vehicle*v = new Vehicle;
			v->load(_fin);
			
			road.addVehicle(*v, toFro);
		}
		else {
			MotorVehicle::MotorType motorTyp = MotorVehicle::DIESEL;
			int number = 0;
			double capacity = 0;
			double startReserves = 0;
			double consumtion = 0;

			getline(_fin, linePart); //motortyp
			istringstream iss(linePart);
			iss >> number;
			motorTyp = typ(number);
			getline(_fin, linePart);//capacity	
			istringstream iss1(linePart);
			iss1 >> capacity;
			getline(_fin, linePart); //startReserves
			istringstream iss2(linePart);
			iss2 >> startReserves;
			getline(_fin, linePart);//consumtion
			istringstream iss3(linePart);
			iss3 >> consumtion;

			MotorVehicle* m = new MotorVehicle(capacity, consumtion, motorTyp);
			m->load(_fin);
			road.addVehicle(*m, toFro);
		}
	}
}


MotorVehicle::MotorType RoadNetwork::typ(int _nump)
{
	switch (_nump) {

	case 0:
		return MotorVehicle::PETROL;

	case 1:
		return MotorVehicle::DIESEL;
	case 2:
		return MotorVehicle::LPG;
	case 3:
		return MotorVehicle::HYDROGEN;
	case 4:
		return MotorVehicle::ELECTRO;
	default:
		return MotorVehicle::PETROL;
	}
}



