#pragma once
#ifndef VEHICLE_H
#define VEHICLE_H

#include <ctime>
#include "Length.h"
#include <fstream>

class Road;

class Vehicle
{
	friend class Road;
public:
	Vehicle();
	~Vehicle();
	// Retrieves the current position (distance from 0) in the given length unit
	virtual double getPosition(Length::Unit unit = Length::METRE) const;
	// Returns the current velocity in metres per second
	virtual double getSpeed() const;
	// Changes the velocity to the given speed in metres per second
	virtual void setSpeed(double newSpeed);
	virtual void save(std::ofstream& _fout)const;
	virtual void load(std::ifstream& _fin);
protected:
	// Position along the way at beginning of the current motion phase in metres.
	double startPosition;
private:
	// Time at beginning of the current motion phase.
	// 
	time_t startTime;
	// Current nominal speed in metres per second (m/s)
	double speed;
	// Refers to the currently used road
	Road* road;
};

#endif /*VEHICLE_H*/
