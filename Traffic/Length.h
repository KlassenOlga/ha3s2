#pragma once
class Length
{
public:
	enum Unit {MILLIMETRE, CENTIMETRE, FOOT, YARD, METRE, KILOMETRE, MILE};
	Length(double val = 0.0, Unit unit = METRE);
	~Length();
	// returns the value in the given unit
	double getValue(Unit unit = METRE);
private:
	// Conversion factor for all units listed in type Unit
	static const double factors[];
	// Length value in metres (SI base unit)
	double value;
};

