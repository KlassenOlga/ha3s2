#include "Road.h"
#include "Junction.h"
#include "RoadNetwork.h"
#include <cassert>
#include <sstream>
using namespace std;
Road::Road(Junction & start, Junction & end, const Polyline2D& course, bool forward, bool backward, string name)
	: startNode(&start)
	, endNode(&end)
	, course(course)
	, name(name)
	, vehiclesTo(nullptr)
	, vehiclesFro(nullptr)
	, length(0)	// TODO initialize from junction distance
{
	if (this->startNode->getLocation() != this->course.getStart()) {
		this->course.insertPoint(this->startNode->getLocation(), 0);
		this->course.removePoint(1);
	}
	if (this->endNode->getLocation() != this->course.getEnd()) {
		unsigned short nPoints = this->course.getNumberOfPoints();
		this->course.insertPoint(this->endNode->getLocation(), nPoints);
		this->course.removePoint(nPoints - 1);
	}
	this->length = this->course.getLength();
	// Anmelden bei den Knoten
	this->startNode->connectTo(*this, false);
	this->endNode->connectTo(*this, true);
	// Anmelden bei Netzwerk(en) der Knoten
	RoadNetwork* pNetwork = this->startNode->getNetwork();
	assert(pNetwork->addRoad(*this));
	if (pNetwork != this->endNode->getNetwork()) {
		assert(this->endNode->getNetwork()->addRoad(*this));
	}
	if (forward) {
		this->vehiclesTo = new VehicleList;
	}
	if (backward) {
		this->vehiclesFro = new VehicleList;
	}
}







Road::~Road()
{
	if (this->vehiclesTo != nullptr) {
		// TODO Wer ist f�r die Entsorgung der Fahrzeuge verantwortlich?
		delete this->vehiclesTo;
	}
	if (this->vehiclesFro != nullptr) {
		// TODO Wer ist f�r die Entsorgung der Fahrzeuge verantwortlich?
		delete this->vehiclesFro;
	}
	// TODO Abmelden von Netwerk

}

string Road::getName() const
{
	return this->name;
}



bool Road::addVehicle(Vehicle & vehicle, bool atStart)
{
	bool done = false;
	VehicleList* vehicles = nullptr;
	if (atStart) {
		vehicles = this->vehiclesTo;
	}
	else {
		vehicles = this->vehiclesFro;
	}
	if (vehicles != nullptr) {
		// Test for duplicity
		for (VehicleList::const_iterator iter = vehicles->cbegin();
			iter != vehicles->cend(); ++iter) {
			if (iter->pVehicle == &vehicle) {
				return false;
			}
		}
		VehicleEntry entry = {&vehicle, vehicle.getPosition()};
		vehicles->push_back(entry);
		// Stra�enverweis in vehicle herstellen
		vehicle.road = this;
		done = true;
	}
	return done;
}

void Road::draw(Drawer2D & drawer) const
{
	this->course.draw(drawer);
}

void Road::save(std::ofstream& fout) const
{
	

	course.save(fout);
	fout << name << endl;
	if (this->vehiclesFro==nullptr)
	{
		fout << 0 << endl;
	}
	else {

		fout << 1 << endl;
	}


	if (this->vehiclesTo == nullptr)
	{
		fout << 0 << endl;
	}
	else{

		fout << 1 << endl;
	}
	fout << startNode->getName() << endl;
	fout << endNode->getName() << endl;

	fout << vehiclesTo->size()<<endl;//Anzahl der Fahrzeuge in der Liste VehiclesTo

	//speichern der Fahrzeuge
	for (auto i=this->vehiclesTo->begin();i!=this->vehiclesTo->end();++i )
	{
		i->pVehicle->save(fout);

		
	}

	fout << vehiclesFro->size();//Anzahl der Fahrzeuge in der Liste Vehicles Fro
	for (auto i = this->vehiclesFro->begin(); i != this->vehiclesFro->end(); ++i)
	{
		i->pVehicle->save(fout);

	}
}

