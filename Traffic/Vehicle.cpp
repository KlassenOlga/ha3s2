#include "Vehicle.h"
#include <sstream>
#include <iostream>
using namespace std;

Vehicle::Vehicle()
	: startPosition(0.0)
	, speed(0)
	, startTime(time(nullptr))
{
}


Vehicle::~Vehicle()
{
}

double Vehicle::getPosition(Length::Unit unit) const
{
	return Length(this->startPosition + speed * difftime(time(nullptr), startTime)).getValue(unit);
}

double Vehicle::getSpeed() const
{
	return this->speed;
}

void Vehicle::setSpeed(double newSpeed)
{
	this->startPosition = this->getPosition();
	this->speed = newSpeed;
	this->startTime = time(nullptr);
}

void Vehicle::save(std::ofstream & _fout) const
{
	_fout << "v" <<endl;
	_fout << this->startPosition << endl;
	_fout << this->getSpeed() << endl;
	_fout << this->startTime << endl;
}

void Vehicle::load(std::ifstream & _fin)
{
	string linePart;
	getline(_fin, linePart);
	istringstream iss(linePart);
	iss>>this->startPosition;
	getline(_fin, linePart);
	istringstream iss1(linePart);
	iss1 >> this->speed;
	istringstream iss2(linePart);
	iss2 >> this->startTime;
	
}
