#ifndef Road_h
#define Road_h

#include <vector>

#include "string.h"

class Vehicle;

class Road {

 public:

    virtual double getLength();

 public:
    string name;

 private:
    double length;


 private:

    /**
     * @element-type Vehicle
     */
    std::vector< Vehicle* > vehicles;
};

#endif // Road_h
