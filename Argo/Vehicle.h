#ifndef Vehicle_h
#define Vehicle_h

#include "time_t.h"

class Road;

class Vehicle {

 public:

    virtual double getPosition();

    virtual double getSpeed();

    virtual void setSpeed(double newSpeed);


 private:
    time_t startTime;
    double startPosition;
    double speed;

 public:

    Road *road;
};

#endif // Vehicle_h
